﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

[RequireComponent(typeof(References))]
public class LoadScript : MonoBehaviour {

    FileInfo originalFile;
    TextAsset textFile;
    TextReader reader;

    public List<string> questions = new List<string>();
    public List<string> answer = new List<string>();
    public List<string> answers = new List<string>();

	// Use this for initialization
	void Start () {
        textFile = (TextAsset)Resources.Load("embedded", typeof(TextAsset));
        reader = new StringReader(textFile.text);

        string lineOfText;
        int lineNumber = 0;
         
        while ((lineOfText = reader.ReadLine()) != null)
        {
            //In theory, every 5 lines should be an question?
            if (lineNumber%5 == 0)
            {
                questions.Add(lineOfText);
                //every 5 lines
            }
            else if (lineNumber % 5 + 1 == 0)
            {
                answer.Add(lineOfText);
                //every line AFTER question
            }
            else
            {
                answers.Add(lineOfText);
                //odd lines
            }
            lineNumber++;
        }
        SendMessage("Gather");
	}
	
}
