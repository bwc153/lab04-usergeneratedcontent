﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
/// <summary>
/// References.cs gatheres references from various objects in the screen.
/// It then stores the references for use in the game at later points. 
/// @author Tiffany Fisher, 9-10-2015
/// </summary>

public class References : MonoBehaviour {

    //Variables to store all information, hidden from designer
    [HideInInspector]
    public Text[] answers;
    [HideInInspector]
    public Image[] correct;
    [HideInInspector]
    public Text answer0;
    [HideInInspector]
    public Text answer1;
    [HideInInspector]
    public Text answer2;
    [HideInInspector]
    public Text answer3;
    [HideInInspector]
    public Image gameOverImage, gameWonImage;
    [HideInInspector]
    public Text gameOverText, gameWonText;
    [HideInInspector]
    public GameObject gameOverButton, gameWonButton;
    [HideInInspector]
    public Button[] buttons;
    [HideInInspector]
    public List<string> sentences = new List<string>();
    [HideInInspector]
    public List<string> clues = new List<string>();

    //Altered
    void Start () {

        //Initialize the arrays to store 52 items, which is the number of letters on the wheel of 
        //fortune board
        answers = new Text[4];
        correct = new Image[4];


        //Gather all of the references from the bottom row
        for (int i = 0; i < 4; i++)
        {
            GameObject obj = GameObject.Find("Bottom Row");
            answers[i] = obj.GetComponentInChildren<Text>();
            correct[i] = obj.GetComponentsInChildren<Image>()[0];
        }

        //Gather references to both hint and score location
        answer0 = GameObject.Find("Answer {0}").GetComponent<Text>();
        answer1 = GameObject.Find("Answer {1}").GetComponent<Text>();
        answer2 = GameObject.Find("Answer {2}").GetComponent<Text>();
        answer3 = GameObject.Find("Answer {3}").GetComponent<Text>();


        //for (int i = 0; i < letters.Length; i++)
        //{
        //    Debug.Log(i);
        //    letters[i].text = i.ToString();
        //}

        //Gather the images, text, and buttons for game over and game won
        gameOverImage = GameObject.Find("GameOver").GetComponent<Image>();
        gameOverText = gameOverImage.gameObject.GetComponentInChildren<Text>();
        gameOverButton = GameObject.Find("GameOverButton");
        gameWonImage = GameObject.Find("GameWon").GetComponent<Image>();
        gameWonText = gameWonImage.gameObject.GetComponentInChildren<Text>();
        gameWonButton = GameObject.Find("GameWonButton");

        //Disable the graphics for game over and game won
        //(You cannot use GameObject.Find to locate a disabled object, so they start
        //enabled and are manually disabled)
        gameOverImage.enabled = false;
        gameOverText.enabled = false;
        gameWonImage.enabled = false;
        gameWonText.enabled = false;
        gameOverButton.SetActive(false);
        gameWonButton.SetActive(false);

        //Gather references to all of the buttons that corrispond with letters
        buttons = GameObject.Find("Buttons").GetComponentsInChildren<Button>();

        //Inform the Engine to begin the game!
        SendMessage("Begin");
    }

    void Gather()
    {
        sentences = GetComponent<LoadScript>().questions;
        clues = GetComponent<LoadScript>().answers;
    }


}
